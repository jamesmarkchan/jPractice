package com.jmc.practice;

/**
 * Static class used to contain implementation of bubble sort.
 * @author James
 *
 */
public class BubbleSort {

	/**
	 * Sort an input array using bubble sort.
	 * @param arr The array to sort
	 * @return The sorted array.
	 */
	static public void sort(int [] arr) {
	    int iterations = 0;
	    int swaps = 0;
	    int swapsThisPass;
	    int tmp;
		for (int i=0; i<arr.length-1; i++) { // for number of elements
			swapsThisPass = 0;
			for (int k=0; k<arr.length-1-i; k++) {
				if (arr[k] > arr[k+1]) {
					// swap values of k and k+1
					tmp = arr[k];
					arr[k] = arr[k+1];
					arr[k+1] = tmp;
					swaps++;
					swapsThisPass++;
				}
				iterations++;
			}
			if (swapsThisPass == 0) {
				//System.out.println("sorted!");
				break;
			}
		}
		System.out.println("Bubble Sort Array Size: " + arr.length);
		System.out.println("Iterations: " + iterations+ " Swaps: "+swaps);
	}
}
