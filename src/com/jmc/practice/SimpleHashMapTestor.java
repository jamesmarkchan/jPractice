package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

/**
 * Unit test for the SimpleHashMap. Referred to as the System Under Test (SUT).
 * Standard HashMap is used to validate results.
 * 
 * @author James Mark Chan
 *
 */
public class SimpleHashMapTestor {
	
	@Test
	public void testPutGetClear() {
		
		// Setup SimpleHashMap to compare with HashMap
		HashMap<Integer,Integer> map = new HashMap<>();
		SimpleHashMap sut = new SimpleHashMap();
		
		// Execute test...
		// Populate and clear key 10
		map.put(10, 1000);
		sut.put(10, 1000);		// Setup
		map.clear();
		sut.clear();
		// Populate keys 1-5 skip key 4
		map.put(1, 11);
		map.put(2, 22);
		map.put(3, 33);
		map.put(5, 55);
		sut.put(1, 11);
		sut.put(2, 22);
		sut.put(3, 33);
		sut.put(5, 55);
		// Remove element 2
		map.put(2, 1000);
		sut.put(2, 1000);
		
		// Verify display contents on console
		System.out.println(map);
		System.out.println(sut);
		
		// Verify both maps have the same content.
		for (int i=0; i<10; i++) {
			assertEquals(map.get(i), sut.get(i));
		}
		assertEquals(map.size(), sut.size());
	}
	
	@Test
	public void testBulk() {
		
		// Setup SimpleHashMap to compare with HashMap
		HashMap<Integer,Integer> map = new HashMap<>();
		SimpleHashMap sut = new SimpleHashMap();
		int numEntries = 100;
		
		// Execute test...
		// Populate keys 0-9
		for (int i=0; i<numEntries; i++) {
			map.put(i, i*10+i);
			sut.put(i, i*10+i);
		}
		// overwrite every fifth key
		for (int i=0; i<numEntries; i+=5) {
			map.put(i, i*10+i+30000);
			sut.put(i, i*10+i+30000);
		}
		// overwrite every even key
		for (int i=0; i<numEntries; i+=2) {
			map.put(i, i*20+i*2);
			sut.put(i, i*20+i*2);
		}
		// remove every 10th key
		for (int i=0; i<numEntries; i+=10) {
			map.remove(i);
			sut.remove(i);
		}
		
		// Verify display contents on console
		System.out.println(map);
		System.out.println(sut);
		
		// Verify both maps have the same content.
		for (int i=0; i<numEntries; i++) {
			assertEquals(map.get(i),sut.get(i));
		}
		assertEquals(map.size(), sut.size());
	}

	/** Same as testBulk but with different bucket sizes. */
	@Test
	public void testBulkVaryBuckets() {
		testBulkSpecifyBuckets(1);
		testBulkSpecifyBuckets(5);
		testBulkSpecifyBuckets(10);
		testBulkSpecifyBuckets(20);
	}

	/** Used by testBulkVaryBuckets() */
	public void testBulkSpecifyBuckets(int bucketSize) {
		
		// Setup SimpleHashMap to compare with HashMap
		HashMap<Integer,Integer> map = new HashMap<>();
		SimpleHashMap sut = new SimpleHashMap(bucketSize);
		int numEntries = 100;
		
		// Execute test...
		// Populate keys 0-9
		for (int i=0; i<numEntries; i++) {
			map.put(i, i*10+i);
			sut.put(i, i*10+i);
		}
		// overwrite every fifth key
		for (int i=0; i<numEntries; i+=5) {
			map.put(i, i*10+i+30000);
			sut.put(i, i*10+i+30000);
		}
		// overwrite every even key
		for (int i=0; i<numEntries; i+=2) {
			map.put(i, i*20+i*2);
			sut.put(i, i*20+i*2);
		}
		// remove every 10th key
		for (int i=0; i<numEntries; i+=10) {
			map.remove(i);
			sut.remove(i);
		}
		
		// Verify display contents on console
		System.out.println(map);
		System.out.println(sut);
		
		// Verify both maps have the same content.
		for (int i=0; i<numEntries; i++) {
			assertEquals(map.get(i),sut.get(i));
		}
		assertEquals(map.size(), sut.size());
	}
	
	@Test
	public void testRemove() {
		
		// Setup SimpleHashMap to compare with HashMap
		HashMap<Integer,Integer> map = new HashMap<>();
		SimpleHashMap sut = new SimpleHashMap();
		
		// Execute test...
		// Populate range of entries
		int numEntries = 15;
		for (int i=0; i<numEntries; i++) {
			map.put(i, i*10+i);
			sut.put(i, i*10+i);
		}
		// Remove even entries
		for (int k=0; k<numEntries; k+=2) {
			map.remove(k);
			sut.remove(k);
		}
		
		// Verify display contents on console
		System.out.println(map);
		System.out.println(sut);
		
		// Verify both maps have the same content.
		for (int i=0; i<numEntries; i++) {
			assertEquals(map.get(i), sut.get(i));
		}
		assertEquals(map.size(), sut.size());
	}
}
