package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class BubbleSortTestor {

	@Test
	public void test() {
		int origin [] = new int [] {0,4,1,99,2,5,88,6,3,101};
		int target [] = Arrays.copyOf(origin, origin.length);
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		
		BubbleSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}

	@Test
	public void test2() {
		int origin [] = new int [] {1,4,1,55,2,44,33,22,303,101};
		int target [] = Arrays.copyOf(origin, origin.length);
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		
		BubbleSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test3() {
		int origin [] = new int [] {90,80,70,60,50,40,30,20,10,0};
		int target [] = Arrays.copyOf(origin, origin.length);
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		
		BubbleSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test4() {
		int origin [] = new int [] {5,6,7,8,9,9,8,7,6,5};
		int target [] = Arrays.copyOf(origin, origin.length);
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		
		BubbleSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
}
