package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class MergeSortTestor {

	/** Test with 10 values hardcoded. */
	@Test
	public void testSort10a() {
		// the original array
		int origin [] = new int [] {1,0,7,5,3, 5,9,10,4,2};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		MergeSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}

	/** Test with 10 values in reverse order. */
	@Test
	public void testSort10b() {
		// the original array
		int origin [] = new int [] {10,9,8,7,6, 5,4,3,2,1};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		MergeSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}

	/**
	 * Test with 100 values...
	 */
	@Test
	public void testSort100() {
		int numValues = 100;
		// the original array
		int origin [] = new int [numValues];
		Random randomGenerator = new Random();
		for (int i=0; i<numValues; i++) {
			origin[i] = randomGenerator.nextInt(100);
		}
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		MergeSort.sort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
}
