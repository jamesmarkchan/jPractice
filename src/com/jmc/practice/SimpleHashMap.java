package com.jmc.practice;

import java.util.ArrayList;

/**
 * HashMap implemented for practice.
 * 
 * @author James Mark Chan
 */
public class SimpleHashMap {

	/** Default size of the hash map. */
	static final int DEFAULT_NUM_BUCKETS = 16;
	
	/** Size of the hash map. */
	int numBuckets = DEFAULT_NUM_BUCKETS;
	
	/** Number of hash Entries. */
	ArrayList<Entry> buckets = new ArrayList<>(numBuckets);
	
	/** Default Constructor. */
	public SimpleHashMap() {
		buckets = new ArrayList<>(numBuckets);
		for (int i =0; i<numBuckets; i++) {
			buckets.add(null);
		}
	}

	/** Constructor allows setting bucket size. */
	public SimpleHashMap(final int numberOfBuckets) {
		numBuckets = numberOfBuckets;
		buckets = new ArrayList<>(numBuckets);
		for (int i =0; i<numBuckets; i++) {
			buckets.add(null);
		}
	}
	
	/** Used to hold key/value pairs and create a one way linked list. */
	class Entry {
		Object key = null;
		Object val = null;
		Entry next = null;
		Entry(Object key, Object val) {
			this.key = key;
			this.val = val;
		}
	}
	
	/** Add or replace a key value entry in the map. */
	public void put(Object key, Object value) {
		
		int hashValue = hash(key);
		Entry entry = buckets.get(hashValue);
		
		// bucket is empty add entry
		if (entry == null) {
			buckets.set(hashValue, new Entry(key, value));
			return;
		}
		
		// check bucket and if present replace
		
		while(entry != null) {
			
			// match update
			if (entry.key == key) {
				entry.val = value;
				return;
			}
			
			// not found, add to end
			if (entry.next == null) {
				entry.next = new Entry(key, value);
				return;
			}
			
			// there are more, point to next node and repeat
			entry = entry.next;
		}
	}
	
	/** Retrieve an object value with the given key. */
	public Object get(Object key) {
		
		// get first entry from bucket
		Entry entry = buckets.get(hash(key));
		// check entries until last entry
		while (entry != null) {
			if (entry.key == key) {
				return entry.val;
			}
			entry = entry.next;
		}
		// no entry found
		return null;
	}
	
	/** Remove entry with key from the map. */
	public Object remove(Object key) {
		Entry entry = buckets.get(hash(key));
		// empty bucket so not present
		if (entry == null) {
			return null;
		}
		
		// match first entry
		if (entry != null && entry.key == key) {
			if (entry.next == null) {
				// only element just remove
				buckets.set(hash(key), null);
				return entry.val;
			}
			
			// more elements remove and point to next
			buckets.set(hash(key), entry.next);
			entry.next = null;
			return entry.val;
		}
		
		// Move to next entry
		Entry prev = entry;
		entry = entry.next;
		
		// Check each entry for match and repeat till end of list
		while (entry != null) {
			if (entry.key == key) {
				prev.next = entry.next;
				entry.next = null;
				return entry.val;
			}
			prev = entry;
			entry = entry.next;
		}
		
		// no element found
		return null;
	}
	
	/** Check if the map contains a key. */
	public boolean contains(Object key) {
		return get(key) == null ? false : true;
	}
	
	/** The main hash function. */
	private int hash(Object key) {
		return key.hashCode() % numBuckets;
	}
	
	/** Clear all the mapping in this map. */
	public void clear() {
		//reinitialize array
		Entry prev;
		Entry entry;
		for (int i = 0; i < numBuckets; i++) {
			entry = buckets.get(i);
			// clear all buckets
			while(entry != null) {
				prev = entry;
				entry = entry.next;
				prev.next = null;
			}
			// reinitialize array
			buckets.set(i,null);
		}
	}
	
	/** Number of entries in this map. */
	public int size() {
		int count = 0;
		for (Entry entry : buckets) {
			while(entry != null) {
				count++;
				entry = entry.next;
			}
		}
		return count;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (int b = 0; b < numBuckets; b++) {
			sb.append("B["+b+"] ");
			Entry e = buckets.get(b);
			while (e != null) {
				sb.append("E("+e.key+","+e.val+") " );
				e = e.next;
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
