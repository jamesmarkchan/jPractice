package com.jmc.practice;

import java.text.SimpleDateFormat;
import java.util.Date;

/** Used to record and display elapsed time to console. */
public class Timer {
    /** Milliseconds in a second. */
    static final int MILLIS_PER_SECOND = 1000;
    /** Start time stamp. */
    Date start = null;
    /** End time stamp. */
    Date end = null;

    /**
     * Record the start time.
     *
     * @return convenience reference.
     */
    public final Timer start() {
        start = new Date();
        return this;
    }

    /**
     * Record the stop time.
     *
     * @return convenience reference.
     */
    public final Timer stop() {
        end = new Date();
        return this;
    }

    /**
     * @return The elapsed time in milliseconds.
     */
    public final long elapsed() {
        if (start == null || end == null) {
            return -1;
        }
        return end.getTime() - start.getTime();
    }

    /** @return The elapsed time in seconds. */
    public final float elapsedSeconds() {
        return (float) elapsed() / (float) MILLIS_PER_SECOND;
    }

    /**
     * Compute and display the elapsed time.
     *
     * @return Formatted string.
     */
    public final String elapsedMessage() {
        long totalMs = elapsed();
        return "ElapsedDuration(mm:ss:SSS) - "
            + (new SimpleDateFormat("mm:ss:SSS")).format(new Date(totalMs));
    }
}