package com.jmc.practice;

import java.util.Arrays;

/** Implementation of quick sort algorithms. */
public class QuickSort {
	
	/** Entry point for the jmc quicksort algorithm. */
	static void jmcSort(int [] arr) {
		jmcQuicksort(arr, 0, arr.length-1);
	}
	
	/** My first take at a quick sort algorithm modeled after Vogella's approach. */
	static void jmcQuicksort(int [] arr, int low, int high) {
		if (low < high) {
			PartitionResult pResult = jmcPartition(arr, low, high);
			jmcQuicksort(arr, low, pResult.highIndex);
			jmcQuicksort(arr, pResult.lowIndex, high);
		}
	}
	
	/** Holder class for returning the high lower block index and the low higher block index. */
	static class PartitionResult {
		public int lowIndex = -1;
		public int highIndex = -1;
		public PartitionResult(int low, int high) {
			this.lowIndex = low;
			this.highIndex = high;
		}
	}
	
	/** The jmc Partition function. */
	static PartitionResult jmcPartition(int [] arr, int low, int high) {
		// select pivot
		int pIndex = (high + low) / 2;
		PartitionResult pResult = new PartitionResult(-1,-1);
		int pValue = arr[pIndex];
		//System.out.println("(lo:"+low+", hi:"+high+" )"+Arrays.toString(arr));
		//System.out.println("pivot["+pIndex+"]: "+pValue);
		for (int i=low, j=high; i<=j;) {
			// move i until we find to an element larger than the pivot value
			while (arr[i] < pValue) {
				i++;
			}
			// move j until we find an element smaller than the pivot value
			while (arr[j] > pValue) {
				j--;
			}
		    if (i<=j) {
		    	if (i!=j) {
		    		swap(arr, i,j);
				}
				i++;
				j--;
				pResult.lowIndex = i;
				pResult.highIndex = j;
			}
		}
		//System.out.println("pivot["+pIndex+"]("+pValue+") "+Arrays.toString(arr)+"\n");
		return pResult;
	}
	
	/** Used by the jmcQuicksort and vQuicksort. */
	static void swap(int [] arr, int a, int b) {
		//System.out.println(" swapping: arr[a:"+a+"]:"+arr[a]+" arr[b:"+b+"]"+arr[b]);		
		int tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}
	

	/** Entry point for Vogella's quick sort algorithm. */
	static void vSort(int[] values) {
		// check for empty or null array
		if (values == null || values.length == 0) {
			return;
		}
		vQuicksort(values, 0, values.length - 1);
	}

	/** Vogella's quick sort algorithm. */
	static void vQuicksort(int [] arr, int low, int high) {
		int i = low, j = high;
		// Get the pivot element from the middle of the list
		int pindex = low + (high - low) / 2;
		int pivot = arr[pindex];
		System.out.println("p["+pindex+"]:"+pivot+"  before partition(lo:"+low+",hi:"+high+"): "+Arrays.toString(arr));
		// Divide into two lists
		while (i <= j) {
			// If the current value from the left list is smaller then the pivot
			// element then get the next element from the left list
			while (arr[i] < pivot) {
				i++;
			}
			// If the current value from the right list is larger then the pivot
			// element then get the next element from the right list
			while (arr[j] > pivot) {
				j--;
			}

			// If we have found a values in the left list which is larger then
			// the pivot element and if we have found a value in the right list
			// which is smaller then the pivot element then we exchange the
			// values.
			// As we are done we can increase i and j
			if (i <= j) {
				System.out.println("exchaning n["+i+"]:"+arr[i]+" and n["+j+"]:"+arr[j]);
				swap(arr, i, j);
				i++;
				j--;
			}
		}
		System.out.println("after partition(i:"+i+" ,j:"+j+"): "+Arrays.toString(arr)+"\n");
		// Recursion
		if (low < j)
			vQuicksort(arr, low, j);
		if (i < high)
			vQuicksort(arr, i, high);
	}

}
