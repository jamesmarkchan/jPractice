package com.jmc.practice;

//import java.util.Arrays;

public class MergeSort {
	
	int [] target; // the target array
	int [] helper; // the helper array
	
	/** Convenience static method. */
	static void sort(int [] arr) {
		new MergeSort().mergeSort(arr);
	}
	
	/** Perform merge sort on an array. Initializes array and helper. */
	void mergeSort(int [] arr) {
		target = arr;
		helper = new int [target.length];
		mergeSort(0,arr.length-1);
	}
	
	/** Perform the recursive component of merge sort. Top down approach. */
	void mergeSort(int low, int high) {
		if (low >= high) { return; }
		
		//System.out.println("the array: "+Arrays.toString(arr));
		int middle = low + (high - low)/2;
		
		//System.out.println("check - sorting lower "+low+":"+middle);
		mergeSort(low, middle);
		//System.out.println("check - sorting upper "+(middle+1)+":"+high);
		mergeSort(middle+1, high);
		//System.out.println("merging "+(high-low+1)+" number of elements ");
		merge (low, middle, high);
	}
	
	/** Perform the merging of the upper and lower sorted arrays. */
	void merge(int low, int middle, int high) {
		
		// copy range of interest into helper array
		for (int i=low; i<=high; i++) {
			helper[i] = target[i];
		}
		
		// while there are values in both the upper and lower (helper)
		// arrays compare and place the lower into the target array.
		int i=low, j=middle+1, k=low;
		while (i<=middle && j<=high) {
			if (helper[i] < helper[j]) {
				target[k++] = helper[i++];
			} else {
				target[k++] = helper[j++];
			}
		}
		
		// copy remaining lower values from helper
		while (i<=middle) {
			target[k++] = helper[i++];			
		}
		
		// copy remaining higher values from helper
		while (j<=high) {
			target[k++] = helper[j++];
		}
		
		//System.out.println("the merged array: "+Arrays.toString(target));
	}
}
