package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

/** 
 * Generally we test against a java api sort function but vogella's version is
 * added as a reference as well.
 *  
 * @author james
 */
public class QuickSortTestor {

	@Test
	public void test0() {
		// the original array
		int origin [] = new int [] {0,1};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		int vogell [] = Arrays.copyOf(origin, origin.length);
		QuickSort.vSort(vogell);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		QuickSort.jmcSort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("vogell array: " + vogell + Arrays.toString(vogell));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test1() {
		// the original array
		int origin [] = new int [] {4,3,2,1};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		int vogell [] = Arrays.copyOf(origin, origin.length);
		QuickSort.vSort(vogell);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		QuickSort.jmcSort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("vogell array: " + vogell + Arrays.toString(vogell));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test() {
		// the original array
		int origin [] = new int [] {0,4,1,99,2,5,88,6,3,101};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		int vogell [] = Arrays.copyOf(origin, origin.length);
		QuickSort.vSort(vogell);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		QuickSort.jmcSort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("vogell array: " + vogell + Arrays.toString(vogell));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test2() {
		// the original array
		int origin [] = new int [] {101,100,7,0,4,1,5};
		
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		// vogella's array
		int vogell [] = Arrays.copyOf(origin, origin.length);
		QuickSort.vSort(vogell);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		QuickSort.jmcSort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("vogell array: " + vogell + Arrays.toString(vogell));
		System.out.println("target array: " + target + Arrays.toString(target));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}
	
	@Test
	public void test3() {
		// the original array
		int origin [] = new int [] {1,2,2,2,1,1,0};
		// this is a sorted array using Array.sort
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Arrays.sort(sorted);
		int vogell [] = Arrays.copyOf(origin, origin.length);
		QuickSort.vSort(vogell);
		// this is the array we operate on
		int target [] = Arrays.copyOf(origin, origin.length);
		QuickSort.jmcSort(target);
		
		System.out.println("origin array: " + origin + Arrays.toString(origin));
		System.out.println("sorted array: " + sorted + Arrays.toString(sorted));
		System.out.println("target array: " + target + Arrays.toString(target));
		System.out.println("vogell array: " + vogell + Arrays.toString(vogell));
		
		assertArrayEquals("target array does not match sorted array", sorted, target);
	}

}
