package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.Test;

public class SimpleTreeMapTestor {

	@Test
	public void testEmptyTree() {
		SimpleTreeMap tree = new SimpleTreeMap();
		tree.clear();
		assertFalse(tree.contains(1));
		assertNull(tree.get(1));
		assertNull(tree.remove(1));
		assertEquals(0, tree.getValues().size());
		assertEquals(0, tree.getKeys().size());
	}
	
	@Test
	public void testPutGetKeysValues() {
		SimpleTreeMap tree = new SimpleTreeMap();
		tree.put(1, 2);
		tree.put(3, 4);
		tree.put(5, 6);
		tree.put(5, 7);
		
		HashSet<Object> keySet = new HashSet<>();
		keySet.add(1);
		keySet.add(3);
		keySet.add(5);
		assertTrue(tree.getKeys().containsAll(keySet));
		
		HashSet<Object> valSet = new HashSet<>();
		valSet.add(2);
		valSet.add(4);
		valSet.add(7);
		assertTrue(tree.getValues().containsAll(valSet));
		
		assertEquals(2, tree.get(1));
		assertEquals(4, tree.get(3));
		assertEquals(7, tree.get(5));
	}
	
	@Test
	public void testRandomKeysValues() {
		
		// Setup SimpleTreeMap and the java TreeMap with random ints to compare against
		final int NUMBER = 10000;
		SimpleTreeMap tree = new SimpleTreeMap();
		TreeMap<Object, Object> jTree = new TreeMap<>();
		int key;
		int val;
		for (int i=0; i<NUMBER; i++) {
			Random ran = new Random();
			key = ran.nextInt(10000) + 1; 
			val = key;
			tree.put(key, val);
			jTree.put(key, val);
		}
		
		// verify both collections key sets are in the same order
		List<Object> myKeyList = new LinkedList<>(tree.getKeys());
		List<Object> jKeyList = new LinkedList<>(jTree.keySet());
		for (int i=0; i<jKeyList.size(); i++) {
			if (myKeyList.get(i).hashCode() != jKeyList.get(i).hashCode()) {
				fail();
			}
		}
		
		// diagnostic output to display key order
		//System.out.println("num tree keys: "+tree.getKeys().size());
		//System.out.println("tree keys: "+tree.getKeys());
		//System.out.println("tree values: "+tree.getValues());
		//System.out.println("num jTree keys: "+jTree.keySet().size());
		//System.out.println("jTree keys: "+jTree.keySet());
		//System.out.println("jTree values: "+jTree.values());
		
		
		// verify contents are the same
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		assertEquals(tree.size(), jTree.size());
		for (Object k : jTree.keySet()) {
			tree.contains(k);
			assertEquals(jTree.get(k), tree.get(k));
		}
		
		// remove all odd keys
		TreeSet<Object> keySet = new TreeSet<>(jTree.keySet());
		for (Object k: keySet) {
			if (k.hashCode() % 2 == 0) {
				jTree.remove(k);
				tree.remove(k);
			}
		}
		// verify all odd keys are removed
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		assertEquals(tree.size(), jTree.size());
		
		// test clear
		tree.clear();
		jTree.clear();
		assertEquals(0, tree.getValues().size());
		assertEquals(0, tree.getKeys().size());
		assertEquals(0, tree.size());
	}
	
	@Test
	public void testRemoveLeaf() {
		SimpleTreeMap tree = new SimpleTreeMap();
		TreeMap<Object, Object> jTree = new TreeMap<>();
		tree.put(1, 2);
		tree.put(3, 4);
		tree.put(5, 6);
		jTree.put(1, 2);
		jTree.put(3, 4);
		jTree.put(5, 6);
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		tree.remove(5);
		jTree.remove(5);
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
	}
	
	@Test
	public void testRemoveOneChild() {
		// setup initial map
		SimpleTreeMap tree = new SimpleTreeMap();
		TreeMap<Object, Object> jTree = new TreeMap<>();
		tree.put(1, 100);
		tree.put(3, 300);
		tree.put(5, 500);
		jTree.put(1, 100);
		jTree.put(3, 300);
		jTree.put(5, 500);
		// verify maps are similar
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		
		// remove middle node for right right tree
		assertEquals(300,tree.remove(3));
		assertEquals(300,jTree.remove(3));
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		
		// remove middle node for left left tree
		tree.put(4, 400);
		tree.put(2, 200);
		jTree.put(4, 400);
		jTree.put(2, 200);
		assertEquals(400,tree.remove(4));
		assertEquals(400,jTree.remove(4));
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		
		// modify tree to we can test the RLR tree combination
		tree.put(3, 300);
		jTree.put(3, 300);
		
		// execute remove on LR tree
		assertEquals(200,tree.remove(2));
		assertEquals(200,jTree.remove(2));
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		
		// execute remove on LR tree
		assertEquals(500,tree.remove(5));
		assertEquals(500,jTree.remove(5));
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
	}
	
	@Test
	public void testRemoveTwoChildren() {
		SimpleTreeMap tree = new SimpleTreeMap();
		TreeMap<Object, Object> jTree = new TreeMap<>();
		tree.put(1, 2);
		tree.put(3, 4);
		tree.put(5, 6);
		tree.put(2, 200);
		tree.put(22, 200);
		tree.put(4, 400);
		
		jTree.put(1, 2);
		jTree.put(3, 4);
		jTree.put(5, 6);
		jTree.put(2, 200);
		jTree.put(22, 200);
		jTree.put(4, 400);
		
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
		tree.remove(3);
		jTree.remove(3);
		assertTrue(tree.getValues().containsAll(jTree.values()));
		assertTrue(tree.getKeys().containsAll(jTree.keySet()));
	}
	
	@Test
	public void testRemoveRoot() {
		SimpleTreeMap tree = new SimpleTreeMap();
		tree.put(1, 1);
		tree.remove(1);
		assertEquals(0, tree.size());
		tree.remove(1);
	}
}
