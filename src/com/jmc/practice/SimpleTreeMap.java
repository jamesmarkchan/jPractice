package com.jmc.practice;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * A simple unbalanced binary tree. Still being implemented.
 * @author James
 *
 */
public class SimpleTreeMap {

	Node root = null;
	
	class Node {
		Node parent = null;
		Node leftChild = null;
		Node rightChild = null;
		Object key = null;
		Object value = null;
		
		public Node(Object key, Object value) {
			this.key = key;
			this.value = value;
		}
		
		public Node(Node parent, Object key, Object value) {
			this.parent = parent;
			this.key = key;
			this.value = value;
		}
		
		public boolean isLeaf() {
			return leftChild == null && rightChild == null;
		}
		
		public boolean hasTwoChildren() {
			 return leftChild != null && rightChild != null; 
		}
		
		public boolean hasOneChild() {
			boolean hasOnlyOne = false;
			if (leftChild == null && rightChild != null) {
				hasOnlyOne = true;
			}
			if (leftChild != null && rightChild == null) {
				hasOnlyOne = true;
			}
			return hasOnlyOne;
		}
		
		public void unlink() {
			parent = null;
			leftChild = null;
			rightChild = null;
		}
		
		@Override
		public String toString() {
			return "Node(key:"+key+",val:"+value+")";
		}
	}

	public boolean contains(Object key) {
		if (get(key) == null) {
			return false;
		}
		return true;
	}
	
	public void clear() {
		root = null;
	}
	
	public int size() {
		return getKeys().size();
	}
	
	public Collection<Object> getKeys() {
		Set<Object> allKeys = new HashSet<>();
		collectKeys(allKeys, root);
		return allKeys;
	}
	
	private void collectKeys(Set<Object> allKeys, Node node) {
		if (node == null) {
			return;
		}
		if (node.leftChild != null) {
			collectKeys(allKeys, node.leftChild);
		}
		allKeys.add(node.key);
		if (node.rightChild != null) {
			collectKeys(allKeys, node.rightChild);
		}
	}
	
	public Collection<Object> getValues() {
		Set<Object> allVals = new HashSet<>();
		collectValues(allVals, root);
		return allVals;
	}
	
	private void collectValues(Set<Object> allVals, Node node) {
		if(node == null) {
			return;
		}
		allVals.add(node.value);
		if (node.leftChild != null) {
			collectValues(allVals, node.leftChild);
		}
		if (node.rightChild != null) {
			collectValues(allVals, node.rightChild);
		}
	}
	
	public void put(Object key, Object value) {
		if (root == null) {
			root = new Node(key, value);
			return;
		} else {
			add(root, key, value);
		}
	}
	
	private void add(Node parent, Object key, Object value) {
		if (key.hashCode() == parent.key.hashCode()) {
			parent.value = value;
		} else if (key.hashCode() > parent.key.hashCode()) {
			if (parent.rightChild == null) {
				parent.rightChild = new Node(parent, key, value);
			} else {
				add(parent.rightChild, key, value);
			}
		} else {
			if (parent.leftChild == null) {
				parent.leftChild = new Node(parent, key,value);
			} else {
				add(parent.leftChild, key, value);
			}
		}
	}

	public Object get(Object key) {
		Node node = getNode(root, key);
		if (node != null) {
			return node.value;
		}
		return null;
	}
	
	private Node getNode(Node node, Object key) {
		//System.out.println("calling getNode on node "+node+","+ "with key: "+key);
		if (node == null) {
			return null;
		}
		if (key.hashCode() == node.key.hashCode()) {
			return node;
		} else if (key.hashCode() > node.key.hashCode()) {
			return getNode(node.rightChild, key);
		} else {
			return getNode(node.leftChild, key);
		}
	}
	
	public Object remove(Object key) {
		Node target = getNode(root, key);
		if (target == null) {
			return null;
		}
		if (target.isLeaf()) {
			// parent does not exist for the root node
			if (target.parent == null) {
				root = null;
			} else {
				// unlink the parent
				if (target.parent.leftChild == target) {
					target.parent.leftChild = null;
				} else {
					target.parent.rightChild = null;
				}
				target.parent = null;
			}
			return target; 
		}
		if (target.hasOneChild()) {
			// get single child
			Node child = null;
			if (target.leftChild != null) {
				child = target.leftChild;
			} else {
				child = target.rightChild;
			}
			// link the child to its new parent
			child.parent = target.parent;
			// link the parent to its new child
			if (target.parent.rightChild == target) {
				target.parent.rightChild = child;
			} else {
				target.parent.leftChild = child;
			}
			// remove target's references. (needed?)
			target.unlink();
		}
		if (target.hasTwoChildren()) {
			// swap with lowest in right sub tree, remove that node
			Node nextHighest = getLowest(target.rightChild);
			// remove the next highest
			remove(nextHighest.key);
			//configure target to be the next highest
			target.key = nextHighest.key;
			target.value = nextHighest.value;
		}
		return target.value;
	}
	
	/**
	 * Returns the lowest key map in the sub tree with specified root
	 * 
	 * @param subTreeRoot
	 * @return The lowest Node
	 */
	private Node getLowest(Node subTreeRoot) {
		Node lowest = subTreeRoot;
		while (lowest.leftChild != null) {
			lowest = lowest.leftChild;
		}
		return lowest;
	}
}
