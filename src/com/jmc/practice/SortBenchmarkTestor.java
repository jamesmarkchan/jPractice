package com.jmc.practice;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class SortBenchmarkTestor {

	/**
	 * Benchmark Different Sort Algorithms. Adjust the numValues to adjust datasize.
	 */
	@Test
	public void testBenchmark() {
		int numValues = 100000;
		int target [] = null;
		
		// generate the original array
		int origin [] = new int [numValues];
		Random randomGenerator = new Random();
		for (int i=0; i<numValues; i++) {
			origin[i] = randomGenerator.nextInt(100);
		}
		
		// This is the Java sort Test.
		int sorted [] = Arrays.copyOf(origin, origin.length);
		Timer tJavaSort = new Timer().start();
		Arrays.sort(sorted);
		String javaSortDuration = tJavaSort.stop().elapsedMessage();
		
		// This is the BubbleSort test
		target = Arrays.copyOf(origin, origin.length);
		Timer tBubbleSort = new Timer().start();
		BubbleSort.sort(target);
		String bubbleSortDuration = tBubbleSort.stop().elapsedMessage();
		assertArrayEquals("bubble sort array does not match java sorted array", sorted, target);
		
		// This is the QuickSort test
		target = Arrays.copyOf(origin, origin.length);
		Timer tQuickSort = new Timer().start();
		QuickSort.jmcSort(target);
		String quickSortDuration = tQuickSort.stop().elapsedMessage();
		assertArrayEquals("quick sort array does not match java sorted array", sorted, target);
		
		// This is the MergeSort test
		target = Arrays.copyOf(origin, origin.length);
		Timer tMergeSort = new Timer().start();
		MergeSort.sort(target);
		String mergeSortDuration = tMergeSort.stop().elapsedMessage();
		assertArrayEquals("merge sort array does not match java sorted array", sorted, target);
		
		System.out.println("java sort duration: "+javaSortDuration);
		System.out.println("bubble sort duration: "+bubbleSortDuration);
		System.out.println("quick sort duration: "+quickSortDuration);
		System.out.println("merge sort duration: "+mergeSortDuration);
		
	}
	
}
